VISION VIAL INTELIGENTE

INTRODUCCION

Este proyecto tiene como objetivo realizar un proceso de investigación, que
demuestre de forma teórica y práctica (a través de un prototipo funcional), las
potencialidades y los beneficios que puede generar la aplicación de tecnologías
emergentes para abordar el problema accidentalidad de las vías en una ciudad.

Especificamente, pretende aplicar vision computacional para, apartir de imagenes
de video recolectadas en las vias, identificar los patrones que generan accidentes
de transito.

DESCRIPCION DEL REPOSITORIO

Este proyecto contiene un cluster funcional Hadoop/Spark con una aplicacion
aprovisionada que implementa algoritmos de vision computacional para la deteccion
de patrones que generan accidentes. Dicho algoritmo esta escrito en python y
usa OpenCV, Darknet, y Yolo3. El cluster esta conformado por dos maquinas virtuales
que corresponden a un node maestro (nodemaster) y un nodo de datos (node1) hadoop.

COMO USAR ESTE REPOSITORIO

1. Descargar VirtualBox desde https://www.virtualbox.org/wiki/Downloads

2. Descargar e instalar desde http://www.vagrantup.com/downloads.html).

3. Ejecutar vagrant up para crear las dos maquinas virtuales

4. Ejecutar vagrant ssh node1 o vagrant ssh nodemaster para ingresar a las
maquinas virtuales

CORRER APLICACION DE PRUEBA

1. Conectarse al nodemaster

vagrant ssh nodemaster

2. Cambiar a usuario hadoop

su hadoop

El password solicitado es hadoop

3. Subir video de prueba al sistema de archivos distribuido (HDFS)

cd /home/hadoop/vision/videoInputs

hdfs dfs -put camara1.mp4 vids

4. Ejecutar aplicacion de vision vial inteligente

cd /home/hadoop/vision/pythonPrograms

./run_vision.sh

5. Los videos resultantes estaran en la carpeta /tmp del node1.

Ejecutar:

ssh node1

cd /tmp

ls

El nombre del video original tendra un prefijo "ori" y el del resultante un
prefijo "rs". Por ejemplo,

ori-4841c4fc-f3f9-11e8-8f7b-0800275f82b1.mp4

rs-4841c4fc-f3f9-11e8-8f7b-0800275f82b1.avi

Para extraer estos videos los puede copiar al directorio sincronizado de Vagrant

Por ejemplo:

cp /vagrant/ori-4841c4fc-f3f9-11e8-8f7b-0800275f82b1.mp4

cp /vagrant/rs-4841c4fc-f3f9-11e8-8f7b-0800275f82b1.avi

De esta manera los videos quedaran disponibles en el directorio raiz del proyecto
en la maquina anfitriona.
