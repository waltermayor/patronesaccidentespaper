#!/bin/bash
# -*- ENCODING: UTF-8 -*-
# script instalación de librerias proyecto MINTIC

# Update ubuntu
echo "Updating Ubuntu"
sudo apt-get update

#Install unzip
echo "Installing unzip"
sudo apt-get install unzip -y

# instalar python2.7
echo "Installing python 2.7"
export LC_ALL=C
sudo apt-get install python-pip python-dev -y
sudo pip install opencv-python
sudo pip install opencv-contrib-python==3.4.1.15

#instalar dependencias de c++ y python para opencv
echo "Installing C++ dependencies"
sudo apt-get install build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev -y

echo "Installing Other libs"
sudo apt-get install python3.5-dev python3-numpy -y 

sudo apt-get install libjpeg-dev libpng-dev libtiff5-dev libjasper-dev libdc1394-22-dev libeigen3-dev libtheora-dev libvorbis-dev libxvidcore-dev libx264-dev sphinx-common libtbb-dev yasm libfaac-dev libopencore-amrnb-dev libopencore-amrwb-dev libopenexr-dev libgstreamer-plugins-base1.0-dev libavutil-dev libavfilter-dev libavresample-dev -y

sudo apt-get install python-numpy libtbb2 libtbb-dev -y   

sudo apt-get install libatlas-base-dev gfortran pylint -y
sudo apt-get install python2.7-dev python3.5-dev -y


#descargar e instalar opencv
#cd /home
echo "Installing OpenCV"
wget https://github.com/opencv/opencv/archive/3.4.0.zip -O opencv-3.4.0.zip
unzip opencv-3.4.0.zip
cd opencv-3.4.0
mkdir build
cd build
sudo cmake ..
sudo make -j5
sudo make install
cd ../..
rm opencv-3.4.0.zip	

#instalar librerias para video
echo "Installing Video Libs"
sudo pip install moviepy
sudo add-apt-repository ppa:mc3man/trusty-media -y
sudo apt-get install ffmpeg -y
sudo apt-get install frei0r-plugins -y
sudo apt-get install python-imaging-tk -y

#Instalar hdfs and pyspark libraries
sudo pip install hdfs
sudo pip install pyspark

#instalar otras librerias
sudo pip install scikit-learn
sudo pip install matplotlib==2.2.3
sudo pip install tensorflow
sudo pip install simplejson
sudo pip install pyyaml

