#!/bin/bash

#Hacer esto por cada datanode
scp -r ../CNN node1:/tmp
scp ../cameraConfig/* node1:/tmp
scp ../frozen_inference_graph.pb node1:/tmp

#Necesario solo si se usa nodemaster como datanode
cp -r ../CNN /tmp
cp ../cameraConfig/* /tmp
cp ../frozen_inference_graph.pb /tmp
rm dependencies.zip
zip dependencies.zip detector.py tracker.py SelectStreets.py structures.py helpers.py darknet.py
